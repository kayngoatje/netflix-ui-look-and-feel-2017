# netflix-ui-look-and-feel-2017

With  this prototype I simulated the Explora&#39;s use of arrow keys on the remote for navigating the UI. I use the computer&#39;s arrow keys(key numbers: 37, 38, 39, 40) to navigate across the different content buckets each loaded using an HTTP GET method.

To quickly scaffold  and run the following SPA. Run the following commands in your CMD step by step.

* npm install
* npm install --global vue-cli (to install vue container)
* npm run build (To build application)
* npm run dev (To run application)