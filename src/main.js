import App from './App.vue'
//import { body } from './templates'


window.onload = function(){ 

// 0. If using a module system, call Vue.use(VueRouter)
// 1. Define route components.
// These can be imported from other files


//const Bar = { template: '<div>bar</div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: App }
  //{ path: '/footer', component: Bar }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes,
  history: 'true',
  mode: 'history'
})

Vue.use(VueRouter)


// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router,
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
}).$mount('#app')

/*
var Profile = Vue.extend({
  template: '<p>{{firstName}} {{lastName}} aka {{alias}}</p>',
  data: function () {
    return {
      firstName: 'Walter',
      lastName: 'White',
      alias: 'Heisenberg'
    }
  }
})
// create an instance of Profile and mount it on an element
new Profile().$mount('#mo')
*/
// Now the app has started!
}

